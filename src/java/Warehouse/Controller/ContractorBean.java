package Warehouse.Controller;

import Warehouse.Config.DBManager;
import Warehouse.Model.Contractor;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

public class ContractorBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Contractor contractor;
    private List<Contractor> contractorList;
    private List<Contractor> filteredContractor;
    private Contractor selectedContractor;

    public Contractor getContractor() {
        return contractor;
    }

    public void setContractor(Contractor contractor) {
        this.contractor = contractor;
    }

    public List<Contractor> getContractorList() {
        if (this.contractorList == null) {
            this.contractorList = getContractorListFromDB();
        }
        return contractorList;
    }

    public void setContractorList(List<Contractor> contractorList) {
        this.contractorList = contractorList;
    }

    public List<Contractor> getFilteredContractor() {
        return filteredContractor;
    }

    public void setFilteredContractor(List<Contractor> filteredContractor) {
        this.filteredContractor = filteredContractor;
    }

    public Contractor getSelectedContractor() {
        return selectedContractor;
    }

    public void setSelectedContractor(Contractor selectedContractor) {
        this.selectedContractor = selectedContractor;
    }

    public String addContractor() {
        try {
            contractor.setEntityDataForAdd();
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            em.persist(contractor);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nazwa nie jest unikalna", null));
            return "AddContractor";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie dodano kontrahenta"));
        contractorList = getContractorListFromDB();
        return "ContractorList";
    }

    public String editContractor() {
        try {
            selectedContractor.setEntityDataForEdit();
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            em.merge(selectedContractor);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nazwa nie jest unikalna", null));
            return "EditContractor";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie edytowano kontrahenta"));
        contractorList = getContractorListFromDB();
        return "ContractorList";
    }

    public String deleteContractor() {
        try {
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            Contractor contractorToDelete = em.find(Contractor.class, selectedContractor.getId());
            selectedContractor = null;
            em.remove(contractorToDelete);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Błąd", "Nie można usunąć kontrahenta"));
            return "ContractorList";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie usunięto kontrahenta"));
        contractorList = getContractorListFromDB();
        return "ContractorList";
    }

    public List<Contractor> getContractorListFromDB() {
        EntityManager em = DBManager.getManager().createEntityManager();
        List list = em.createNamedQuery("Contractor.findAll").getResultList();
        em.close();
        return list;
    }

    public List<Contractor> getContractorSellerFromDB() {
        EntityManager em = DBManager.getManager().createEntityManager();
        List list = em.createNamedQuery("Contractor.findSeller").getResultList();
        em.close();
        return list;
    }

    public List<Contractor> getContractorBuyerFromDB() {
        EntityManager em = DBManager.getManager().createEntityManager();
        List list = em.createNamedQuery("Contractor.findBuyer").getResultList();
        em.close();
        return list;
    }

    public String editContractorNavigation() {
        if (selectedContractor != null) {
            return "EditContractor";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Błąd", "Nie można edytować kontrahenta"));
            return "ContractorList";
        }
    }

    public String addContractorNaviation() {
        contractor = new Contractor();
        return "AddContractor";
    }
    
     public String contractorListNavigation() {
        contractorList = getContractorListFromDB();
        return "ContractorListRedirect";
     }
}
