package Warehouse.Controller;

import Warehouse.Config.DBManager;
import Warehouse.Exception.NoDocumentPositionException;
import Warehouse.Model.DocumentPosition;
import Warehouse.Model.PZDocument;
import Warehouse.Model.Stock;
import Warehouse.Model.Warehouse;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

public class PZDocumentBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private PZDocument pzDocument;
    private List<PZDocument> pzDocumentList;
    private List<PZDocument> filteredPZDocument;
    private PZDocument selectedPZDocument;

    public PZDocumentBean() {
        pzDocument = new PZDocument();
    }

    public PZDocument getPzDocument() {
        return pzDocument;
    }

    public void setPzDocument(PZDocument pzDocument) {
        this.pzDocument = pzDocument;
    }

    public List<PZDocument> getPzDocumentList() {
        if (this.pzDocumentList == null) {
            this.pzDocumentList = getPZDocumentListFromDB();
        }
        return pzDocumentList;
    }

    public void setPzDocumentList(List<PZDocument> pzDocumentList) {
        this.pzDocumentList = pzDocumentList;
    }

    public List<PZDocument> getFilteredPZDocument() {
        return filteredPZDocument;
    }

    public void setFilteredPZDocument(List<PZDocument> filteredPZDocument) {
        this.filteredPZDocument = filteredPZDocument;
    }

    public PZDocument getSelectedPZDocument() {
        return selectedPZDocument;
    }

    public void setSelectedPZDocument(PZDocument selectedPZDocument) {
        this.selectedPZDocument = selectedPZDocument;
    }

    public String addPZDocument() {
        try {
            validatePZDocument();
            pzDocument.setEntityDataForAdd();
            pzDocument.setUser();
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            pzDocument.setDocumentNumber(CounterBean.CounterForPZDocument(em));
            em.persist(pzDocument);
            updateStocks(em);
            em.getTransaction().commit();
            em.close();
        } catch (NoDocumentPositionException e) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Dokument musi posiadać conajmniej jedną pozycję", null));
            return "AddPZDocument";
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nie udało się dodać dokumentu", null));
            return "AddPZDocument";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie dodano dokument"));
        pzDocumentList = getPZDocumentListFromDB();
        return "PZDocumentList";
    }

    public String addPZDocumentNavigation() {
        pzDocument = new PZDocument();
        return "AddPZDocument";
    }

    public String viewPZDocumentNavigation() {
        if (selectedPZDocument != null) {
            return "ViewPZDocument";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Błąd", "Nie można pzejrzeć dokumentu"));
            return "PZDocumentList";
        }
    }

    public List<PZDocument> getPZDocumentListFromDB() {
        EntityManager em = DBManager.getManager().createEntityManager();
        List list = em.createNamedQuery("PZDocument.findAll").getResultList();
        em.close();
        return list;
    }

    public void updateStocks(EntityManager em) {
        Warehouse warehouse = pzDocument.getWarehouse();
        for (DocumentPosition documentPosition : pzDocument.getDocumentPositions()) {
            List<Stock> stockList = em.createNamedQuery("Stock.findByWarehouseAndProduct").setParameter("WarehouseId", warehouse.getId()).setParameter("ProductId", documentPosition.getProduct().getId()).getResultList();
            if (stockList.size() > 0) {
                Stock stock = stockList.get(0);
                int actual = stock.getQuantity();
                actual += documentPosition.getCount();
                stock.setQuantity(actual);
                em.merge(stock);
            } else {
                Stock createdStock = new Stock(warehouse, documentPosition.getProduct(), documentPosition.getCount());
                em.persist(createdStock);
            }
        }
    }

    public String pzDocumentListNavigation() {
        pzDocumentList = getPZDocumentListFromDB();
        return "PZDocumentListRedirect";
    }

    private void validatePZDocument() throws NoDocumentPositionException {
        if (pzDocument.getDocumentPositions().isEmpty()) {
            throw new NoDocumentPositionException();
        }
    }
}
