package Warehouse.Controller;

import Warehouse.Config.DBManager;
import Warehouse.Config.RoleNames;
import Warehouse.Model.Permission;
import Warehouse.Model.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

public class PermissionBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<Permission> listOfPermissions;

    public PermissionBean() {
        EntityManager em = DBManager.getManager().createEntityManager();
        listOfPermissions = em.createNamedQuery("Permissions.findAll").getResultList();
    }

    public List<Permission> getListOfPermissions() {
        return listOfPermissions;
    }

    public void setListOfPermissions(List<Permission> listOfPermissions) {
        this.listOfPermissions = listOfPermissions;
    }

    private void CreateRoles() {
        List<Permission> listOfRoles = new ArrayList<Permission>();
        RoleNames roleNames = new RoleNames();
        Permission r = new Permission();
        r.setName(roleNames.getDodajPodatek());
        listOfRoles.add(r);

        Permission r2 = new Permission();
        r2.setName(roleNames.getUsunPodatek());
        listOfRoles.add(r2);

        Permission r3 = new Permission();
        r3.setName(roleNames.getDodajJednostke());
        listOfRoles.add(r3);

        Permission r4 = new Permission();
        r4.setName(roleNames.getUsunJednostke());
        listOfRoles.add(r4);

        Permission r5 = new Permission();
        r5.setName(roleNames.getDodajUzytkownika());
        listOfRoles.add(r5);

        Permission r6 = new Permission();
        r6.setName(roleNames.getEdytujUzytkownika());
        listOfRoles.add(r6);

        Permission r7 = new Permission();
        r7.setName(roleNames.getUsunUzytkownika());
        listOfRoles.add(r7);

        Permission r8 = new Permission();
        r8.setName(roleNames.getDodajKontrahenta());
        listOfRoles.add(r8);

        Permission r9 = new Permission();
        r9.setName(roleNames.getEdytujKontrahenta());
        listOfRoles.add(r9);

        Permission r10 = new Permission();
        r10.setName(roleNames.getUsunKontrahenta());
        listOfRoles.add(r10);

        Permission r11 = new Permission();
        r11.setName(roleNames.getDodajTowar());
        listOfRoles.add(r11);

        Permission r12 = new Permission();
        r12.setName(roleNames.getEdytujTowar());
        listOfRoles.add(r12);

        Permission r13 = new Permission();
        r13.setName(roleNames.getUsunTowar());
        listOfRoles.add(r13);

        Permission r14 = new Permission();
        r14.setName(roleNames.getDodajMagazyn());
        listOfRoles.add(r14);

        Permission r15 = new Permission();
        r15.setName(roleNames.getEdytujMagazyn());
        listOfRoles.add(r15);

        Permission r16 = new Permission();
        r16.setName(roleNames.getUsunMagazyn());
        listOfRoles.add(r16);

        Permission r17 = new Permission();
        r17.setName(roleNames.getUprawnienia());
        listOfRoles.add(r17);

        Permission r18 = new Permission();
        r18.setName(roleNames.getDodajDokumentPZ());
        listOfRoles.add(r18);

        Permission r19 = new Permission();
        r19.setName(roleNames.getPrzegladajDokumentPZ());
        listOfRoles.add(r19);

        Permission r20 = new Permission();
        r20.setName(roleNames.getDodajDokumentWZ());
        listOfRoles.add(r20);

        Permission r21 = new Permission();
        r21.setName(roleNames.getPrzegladajDokumentWZ());
        listOfRoles.add(r21);

        Permission r22 = new Permission();
        r22.setName(roleNames.getDodajDokumentMM());
        listOfRoles.add(r22);

        Permission r23 = new Permission();
        r23.setName(roleNames.getPrzegladajDokumentMM());
        listOfRoles.add(r23);

        this.SavePermissionsInDB(listOfRoles);
    }

    private void SavePermissionsInDB(List<Permission> listOfRoles) {
        try {
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            for (Permission role : listOfRoles) {
                em.persist(role);
            }
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
        }
    }

    public void InitPermissions() {
        if (this.listOfPermissions.isEmpty()) {
            this.CreateRoles();
            EntityManager em = DBManager.getManager().createEntityManager();
            while (listOfPermissions.isEmpty()) {
                listOfPermissions = em.createNamedQuery("Permissions.findAll").getResultList();
            }
        }
    }

    public boolean IsInRole(String roleName) {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        LoginBean loginBean = (LoginBean) req.getSession().getAttribute("LoginBean");
        User user = loginBean.getCurrentUser();
        for (Permission role : user.getPermissions()) {
            if (role.getName().equals(roleName)) {
                return true;
            }
        }
        return false;
    }

    private boolean IsSelectedUser() {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserBean userBean = (UserBean) req.getSession().getAttribute("UserBean");
        if (userBean.getSelectedUser() != null) {
            return true;
        } else {
            return false;
        }
    }

    public List<Permission> GetRolesSelectedUser() {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserBean userBean = (UserBean) req.getSession().getAttribute("UserBean");
        return userBean.getSelectedUser().getPermissions();
    }

    public String EditRoles() {
        if (IsSelectedUser()) {
            EntityManager em = DBManager.getManager().createEntityManager();
            this.listOfPermissions = em.createNamedQuery("Permissions.findAll").getResultList();
            List<Permission> listOfRolesSelectedUser = GetRolesSelectedUser();
            for (Permission permission : listOfPermissions) {
                if (listOfRolesSelectedUser.contains(permission)) {
                    permission.setIsSelected(true);
                }
            }
            return "RolesEdit";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Błąd", "Nie można edytować uprawnień"));
            return "UserList";
        }
    }

    public String AddRoles() {
        try {
            HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            UserBean userBean = (UserBean) req.getSession().getAttribute("UserBean");
            User user = userBean.getSelectedUser();
            List<Permission> rolesForUser = new ArrayList<Permission>();
            for (Permission role : this.listOfPermissions) {
                if (role.isIsSelected() == true) {
                    rolesForUser.add(role);
                }
            }
            user.setEntityDataForEdit();
            user.setPermissions(rolesForUser);
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            em.merge(user);
            em.getTransaction().commit();
            em.close();
            this.ChangeUserInSession(user);
        } catch (Exception ex) {
        }
        return "UserListRedirect";
    }

    public void ChangeUserInSession(User user) {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        LoginBean loginBean = (LoginBean) req.getSession().getAttribute("LoginBean");
        User currentUser = loginBean.getCurrentUser();
        if (currentUser.getLogin().equals(user.getLogin())) {
            loginBean.setCurrentUser(user);
            req.getSession().setAttribute("LoginBean", loginBean);
        }
    }
}
