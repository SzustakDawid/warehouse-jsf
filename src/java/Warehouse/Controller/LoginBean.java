package Warehouse.Controller;

import Warehouse.Config.DBManager;
import static Warehouse.Controller.UserBean.HashMD5;
import Warehouse.Model.User;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

public class LoginBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private User currentUser;
    private User tmpUser;

    public LoginBean() {
        this.tmpUser = new User();
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public User getTmpUser() {
        return tmpUser;
    }

    public void setTmpUser(User tmpUser) {
        this.tmpUser = tmpUser;
    }

    public String LogIn() {
        try {
            tmpUser.setPassword(UserBean.HashMD5(tmpUser.getPassword()));
            this.CheckExistAdmin();
            EntityManager em = DBManager.getManager().createEntityManager();
            List<User> listOfUser = em.createNamedQuery("User.findByLogin").setParameter("Login", this.tmpUser.getLogin()).getResultList();

            if (listOfUser.size() > 0) {
                User existUser = listOfUser.get(0);
                if (existUser.getPassword().equals(tmpUser.getPassword())) {
                    currentUser = existUser;
                    existUser = null;
                    tmpUser = new User();
                    return "Home";
                } else {
                    FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nieprawidłowy login lub hasło", null));
                    return "index";
                }
            } else {
                FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nieprawidłowy login lub hasło", null));
                return "index";
            }
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Brak połączenia z serwerem", null));
            return "index";
        }
    }

    public String LogOut() {
        currentUser = null;
        tmpUser = new User();
        return "index";
    }

    public void CheckExistAdmin() {
        EntityManager em = DBManager.getManager().createEntityManager();
        List<User> existAdmin = em.createNamedQuery("User.findByLogin").setParameter("Login", "Admin").getResultList();
        if (existAdmin.isEmpty()) {
            PermissionBean roleBean = new PermissionBean();
            roleBean.InitPermissions();
            this.addAdminUser();
        }
    }

    public void addAdminUser() {
        try {
            User user = new User();
            user.setLogin("Admin");
            user.setFirstName("Administrator");
            user.setLastName("Administrator");
            user.setPassword(HashMD5("admin"));
            Date date = new Date();
            user.setUpdatedAt(date);
            user.setCreatedAt(date);
            EntityManager em = DBManager.getManager().createEntityManager();
            user.setPermissions(em.createNamedQuery("Permissions.findAll").getResultList());
            em.getTransaction().begin();
            em.persist(user);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
        }
    }
}
