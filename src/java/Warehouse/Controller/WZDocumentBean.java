package Warehouse.Controller;

import Warehouse.Config.DBManager;
import Warehouse.Exception.NoDocumentPositionException;
import Warehouse.Exception.SameProductException;
import Warehouse.Exception.TooMuchCountException;
import Warehouse.Model.DocumentPosition;
import Warehouse.Model.Product;
import Warehouse.Model.Stock;
import Warehouse.Model.WZDocument;
import Warehouse.Model.Warehouse;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

public class WZDocumentBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private WZDocument wzDocument;
    private List<WZDocument> wzDocumentList;
    private List<WZDocument> filteredWZDocument;
    private WZDocument selectedWZDocument;
    private Stock selectedStock;
    private String TxtWarehouseId;
    private List<Stock> filteredStock;
    private List<Stock> stockList;
    private Product productFromSelectedStock;

    public WZDocument getWzDocument() {
        return wzDocument;
    }

    public void setWzDocument(WZDocument wzDocument) {
        this.wzDocument = wzDocument;
    }

    public List<WZDocument> getWzDocumentList() {
        if (wzDocumentList == null) {
            wzDocumentList = getWZDocumentListFromDB();
        }
        return wzDocumentList;
    }

    public void setWzDocumentList(List<WZDocument> wzDocumentList) {
        this.wzDocumentList = wzDocumentList;
    }

    public List<WZDocument> getFilteredWZDocument() {
        return filteredWZDocument;
    }

    public void setFilteredWZDocument(List<WZDocument> filteredWZDocument) {
        this.filteredWZDocument = filteredWZDocument;
    }

    public WZDocument getSelectedWZDocument() {
        return selectedWZDocument;
    }

    public void setSelectedWZDocument(WZDocument selectedWZDocument) {
        this.selectedWZDocument = selectedWZDocument;
    }

    public Stock getSelectedStock() {
        return selectedStock;
    }

    public void setSelectedStock(Stock selectedStock) {
        this.selectedStock = selectedStock;
        this.productFromSelectedStock = selectedStock.getProduct();
    }

    public String getTxtWarehouseId() {
        return TxtWarehouseId;
    }

    public void setTxtWarehouseId(String TxtWarehouseId) {
        this.TxtWarehouseId = TxtWarehouseId;
    }

    public List<Stock> getFilteredStock() {
        return filteredStock;
    }

    public void setFilteredStock(List<Stock> filteredStock) {
        this.filteredStock = filteredStock;
    }

    public List<Stock> getStockList() {
        return stockList;
    }

    public void setStockList(List<Stock> stockList) {
        this.stockList = stockList;
    }

    public Product getProductFromSelectedStock() {
        return productFromSelectedStock;
    }

    public String AddWZDocumentNavigation() {
        wzDocument = new WZDocument();
        EntityManager em = DBManager.getManager().createEntityManager();
        List<Warehouse> list = em.createNamedQuery("Warehouse.findAll").setMaxResults(1).getResultList();
        if (!list.isEmpty()) {
            TxtWarehouseId = list.get(0).getId().toString();
        }
        getStockForWarehouseFromDB();
        return "AddWZDocument";
    }

    public String AddWZDocument() {
        try {
            validateWZDocument();
            wzDocument.setEntityDataForAdd();
            wzDocument.setUser();
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            wzDocument.setDocumentNumber(CounterBean.CounterForWZDocument(em));
            em.persist(wzDocument);
            updateStocks(em);
            em.getTransaction().commit();
            em.close();
        } catch (SameProductException e) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Towary na liście, muszą być unikalne", null));
            return "AddWZDocument";
        } catch (TooMuchCountException e) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nie można wydać więcej towaru niż, jest w magazynie", null));
            return "AddWZDocument";
        } catch (NoDocumentPositionException e) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Dokument musi posiadać conajmniej jedną pozycję", null));
            return "AddWZDocument";
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nie udało się dodać dokumentu", null));
            return "AddWZDocument";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie dodano dokument"));
        wzDocumentList = getWZDocumentListFromDB();
        return "WZDocumentList";
    }

    public String viewWZDocumentNavigation() {
        if (selectedWZDocument != null) {
            return "ViewWZDocument";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Błąd", "Nie można pzejrzeć dokumentu"));
            return "WZDocumentList";
        }
    }

    public String wzDocumentListNavigation() {
        wzDocumentList = getWZDocumentListFromDB();
        return "WZDocumentListRedirect";
    }

    public List<WZDocument> getWZDocumentListFromDB() {
        EntityManager em = DBManager.getManager().createEntityManager();
        List list = em.createNamedQuery("WZDocument.findAll").getResultList();
        em.close();
        return list;
    }

    public final String getStockForWarehouseFromDB() {
        try {
            int warId = Integer.parseInt(TxtWarehouseId);
            EntityManager em = DBManager.getManager().createEntityManager();
            List list = em.createNamedQuery("Stock.findForWarehouse").setParameter("WarehouseId", warId).getResultList();
            em.close();
            stockList = list;
        } catch (Exception ex) {
            stockList = new ArrayList<Stock>();
        }
        return null;
    }

    private void validateWZDocument() throws NoDocumentPositionException, TooMuchCountException, SameProductException {
        List productList = new ArrayList<Product>();
        if (wzDocument.getDocumentPositions().isEmpty()) {
            throw new NoDocumentPositionException();
        } else {
            EntityManager em = DBManager.getManager().createEntityManager();
            for (DocumentPosition dp : wzDocument.getDocumentPositions()) {
                int productId = dp.getProduct().getId();
                int warehouseId = wzDocument.getWarehouse().getId();
                List<Stock> list = em.createNamedQuery("Stock.findByWarehouseAndProduct").setParameter("WarehouseId", warehouseId).setParameter("ProductId", productId).getResultList();
                if (list.get(0).getQuantity() < dp.getCount()) {
                    throw new TooMuchCountException();
                }
                if (!productList.contains(dp.getProduct())) {
                    productList.add(dp.getProduct());
                } else {
                    throw new SameProductException();
                }
            }
        }
    }
    

    public void updateStocks(EntityManager em) {
        for (DocumentPosition dp : wzDocument.getDocumentPositions()) {
            int productId = dp.getProduct().getId();
            int warehouseId = wzDocument.getWarehouse().getId();
            List<Stock> list = em.createNamedQuery("Stock.findByWarehouseAndProduct").setParameter("WarehouseId", warehouseId).setParameter("ProductId", productId).getResultList();
            Stock stock = list.get(0);
            stock.setQuantity(stock.getQuantity() - dp.getCount());
            em.merge(stock);
        }
        StockBean.deleteNoQuantity(em);
    }

    public void removeSelectedStock() {
        selectedStock = null;
        productFromSelectedStock = null;
    }
}
