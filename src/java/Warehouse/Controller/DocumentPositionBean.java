package Warehouse.Controller;

import Warehouse.Model.DocumentPosition;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DocumentPositionBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<DocumentPosition> documentPositions;
    private DocumentPosition documentPosition;

    public DocumentPositionBean() {
        documentPosition = new DocumentPosition();
        documentPositions = new ArrayList<DocumentPosition>();
    }

    public List<DocumentPosition> getDocumentPositions() {
        return documentPositions;
    }

    public void setDocumentPositions(List<DocumentPosition> documentPositions) {
        this.documentPositions = documentPositions;
    }

    public DocumentPosition getDocumentPosition() {
        return documentPosition;
    }

    public void setDocumentPosition(DocumentPosition documentPosition) {
        this.documentPosition = documentPosition;
    }

    public void ReinitDocumentPosition() {
        documentPosition = new DocumentPosition();
    }

    public void ReinitDocumentPositions() {
        documentPosition = new DocumentPosition();
        documentPositions = new ArrayList<DocumentPosition>();
    }
}
