package Warehouse.Controller;

import Warehouse.Config.DBManager;
import Warehouse.Model.Unit;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

public class UnitBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Unit unit;
    private List<Unit> unitList;
    private List<Unit> filteredUnit;
    private Unit selectedUnit;

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public List<Unit> getUnitList() {
        if (this.unitList == null) {
            this.unitList = getUnitListFromDB();
        }
        return unitList;
    }

    public void setUnitList(List<Unit> unitList) {
        this.unitList = unitList;
    }

    public List<Unit> getFilteredUnit() {
        return filteredUnit;
    }

    public void setFilteredUnit(List<Unit> filteredUnit) {
        this.filteredUnit = filteredUnit;
    }

    public Unit getSelectedUnit() {
        return selectedUnit;
    }

    public void setSelectedUnit(Unit selectedUnit) {
        this.selectedUnit = selectedUnit;
    }

    public List<Unit> getUnitListFromDB() {
        EntityManager em = DBManager.getManager().createEntityManager();
        List list = em.createNamedQuery("Unit.findAll").getResultList();
        em.close();
        return list;
    }

    public String addUnitNavigation() {
        unit = new Unit();
        return "AddUnit";
    }

    public String addUnit() {
        try {
            unit.setEntityDataForAdd();
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            em.persist(unit);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nazwa nie jest unikalna", null));
            return "AddUnit";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie dodano jednostkę"));
        unitList = getUnitListFromDB();
        return "UnitList";
    }

    public String deleteUnit() {
        try {
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            Unit unitToDelete = em.find(Unit.class, selectedUnit.getId());
            selectedUnit = null;
            em.remove(unitToDelete);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Błąd", "Nie można usunąć jednostki"));
            return "UnitList";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie usunięto jednostkę"));
        unitList = getUnitListFromDB();
        return "UnitList";
    }

    public String unitListNavigation() {
        unitList = getUnitListFromDB();
        return "UnitListRedirect";
    }
}
