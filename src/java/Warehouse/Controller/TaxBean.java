package Warehouse.Controller;

import Warehouse.Config.DBManager;
import Warehouse.Model.Tax;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

public class TaxBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Tax tax;
    private List<Tax> taxList;
    private List<Tax> filteredTax;
    private Tax selectedTax;

    public Tax getTax() {
        return tax;
    }

    public void setTax(Tax tax) {
        this.tax = tax;
    }

    public List<Tax> getTaxList() {
        if (this.taxList == null) {
            this.taxList = getTaxListFromDB();
        }
        return taxList;
    }

    public void setTaxList(List<Tax> taxList) {
        this.taxList = taxList;
    }

    public List<Tax> getFilteredTax() {
        return filteredTax;
    }

    public void setFilteredTax(List<Tax> filteredTax) {
        this.filteredTax = filteredTax;
    }

    public Tax getSelectedTax() {
        return selectedTax;
    }

    public void setSelectedTax(Tax selectedTax) {
        this.selectedTax = selectedTax;
    }

    public String addTax() {
        try {
            tax.setEntityDataForAdd();
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            em.persist(tax);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Wartość nie jest unikalna", null));
            return "AddTax";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie dodano podatek"));
        taxList = getTaxListFromDB();
        return "TaxList";
    }

    public String deleteTax() {
        try {
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            Tax taxToDelete = em.find(Tax.class, selectedTax.getId());
            selectedTax = null;
            em.remove(taxToDelete);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Błąd", "Nie można usunąć podatku"));
            return "TaxList";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie usunięto podatek"));
        taxList = getTaxListFromDB();
        return "TaxList";
    }

    public List<Tax> getTaxListFromDB() {
        EntityManager em = DBManager.getManager().createEntityManager();
        List list = em.createNamedQuery("Tax.findAll").getResultList();
        em.close();
        return list;
    }

    public String addTaxNavigation() {
        tax = new Tax();
        return "AddTax";
    }

    public String taxListNavigation() {
        taxList = getTaxListFromDB();
        return "TaxListRedirect";
    }
}
