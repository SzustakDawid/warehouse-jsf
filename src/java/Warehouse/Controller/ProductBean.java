package Warehouse.Controller;

import Warehouse.Config.DBManager;
import Warehouse.Model.Product;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

public class ProductBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Product product;
    private List<Product> productList;
    private List<Product> filteredProduct;
    private Product selectedProduct;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<Product> getProductList() {
        if (this.productList == null) {
            this.productList = getProductListFromDB();
        }
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public List<Product> getFilteredProduct() {
        return filteredProduct;
    }

    public void setFilteredProduct(List<Product> filteredProduct) {
        this.filteredProduct = filteredProduct;
    }

    public Product getSelectedProduct() {
        return selectedProduct;
    }

    public void setSelectedProduct(Product selectedProduct) {
        this.selectedProduct = selectedProduct;
    }

    public String addProduct() {
        try {
            product.setEntityDataForAdd();
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            em.persist(product);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Symbol nie jest unikalny", null));
            return "AddProduct";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie dodano towar"));
        productList = getProductListFromDB();
        return "ProductList";
    }

    public String editProduct() {
        try {
            selectedProduct.setEntityDataForEdit();
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            em.merge(selectedProduct);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Symbol nie jest unikalny", null));
            return "EditProduct";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie edytowano towar"));
        productList = getProductListFromDB();
        return "ProductList";
    }

    public String deleteProduct() {
        try {
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            Product productToDelete = em.find(Product.class, selectedProduct.getId());
            selectedProduct = null;
            em.remove(productToDelete);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Błąd", "Nie można usunąć towaru"));
            return "ProductList";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie usunięto towar"));
        productList = getProductListFromDB();
        return "ProductList";
    }

    public List<Product> getProductListFromDB() {
        EntityManager em = DBManager.getManager().createEntityManager();
        List list = em.createNamedQuery("Products.findAll").getResultList();
        em.close();
        return list;
    }

    public String editProductNavigation() {
        if (selectedProduct != null) {
            return "EditProduct";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Błąd", "Nie można edytować towaru"));
            return "ProductList";
        }
    }

    public String addProductNavigation() {
        product = new Product();
        return "AddProduct";
    }

    public String productListNavigation() {
        productList = getProductListFromDB();
        return "ProductListRedirect";
    }
}
