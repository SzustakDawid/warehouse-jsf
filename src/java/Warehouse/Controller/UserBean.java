package Warehouse.Controller;

import Warehouse.Config.DBManager;
import Warehouse.Model.User;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

public class UserBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private User user;
    private List<User> userList;
    private List<User> filteredUser;
    private User selectedUser;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getUserList() {
        if (this.userList == null) {
            this.userList = getUserListFromDB();
        }
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public List<User> getFilteredUser() {
        return filteredUser;
    }

    public void setFilteredUser(List<User> filteredUser) {
        this.filteredUser = filteredUser;
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }

    public String addUser() {
        try {
            user.setEntityDataForAdd();
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            user.setPassword(HashMD5(user.getPassword()));
            em.persist(user);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login nie jest unikalny", null));
            return "AddUser";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie dodano użytkownika"));
        userList = getUserListFromDB();
        return "UserList";
    }

    public String editUser() {
        try {
            selectedUser.setEntityDataForEdit();
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            em.merge(selectedUser);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login nie jest unikalny", null));
            return "EditUser";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie edytowano użytkownika"));
        userList = getUserListFromDB();
        return "UserList";
    }

    public String deleteUser() {
        try {
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            User userToDelete = em.find(User.class, selectedUser.getId());
            selectedUser = null;
            em.remove(userToDelete);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Błąd", "Nie można usunąć użytkownika"));
            return "UserList";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie usunięto użytkownika"));
        userList = getUserListFromDB();
        return "UserList";
    }

    public String changePassword() {
        try {
            HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            LoginBean loginBean = (LoginBean) req.getSession().getAttribute("LoginBean");
            User currentUser = loginBean.getCurrentUser();
            User tmpUser = loginBean.getTmpUser();
            currentUser.setPassword(HashMD5(tmpUser.getPassword()));
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            em.merge(currentUser);
            em.getTransaction().commit();
            em.close();
            tmpUser = new User();
        } catch (Exception ex) {
          FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nie udało się zmienić hasła", null));
            return "ChangePassword";
        }
        return "ChangePasswordSucceed";
    }

    public String editOwnData() {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        LoginBean loginBean = (LoginBean) req.getSession().getAttribute("LoginBean");
        User currentUser = loginBean.getCurrentUser();
        this.selectedUser = currentUser;
        return editUserNavigation();
    }

    public List<User> getUserListFromDB() {
        EntityManager em = DBManager.getManager().createEntityManager();
        List list = em.createNamedQuery("User.findAll").getResultList();
        em.close();
        return list;
    }

    public String editUserNavigation() {
        if (selectedUser != null) {
            return "EditUser";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Błąd", "Nie można edytować użytkownika"));
            return "UserList";
        }
    }

    public String addUserNavigation() {
        user = new User();
        return "AddUser";
    }

    public String userListNavigation() {
        userList = getUserListFromDB();
        return "UserListRedirect";
    }

    public static String HashMD5(String password) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        md5.update(password.getBytes());
        BigInteger hash = new BigInteger(1, md5.digest());
        return hash.toString(32);
    }
}
