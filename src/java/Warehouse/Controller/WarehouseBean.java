package Warehouse.Controller;

import Warehouse.Config.DBManager;
import Warehouse.Model.Warehouse;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

public class WarehouseBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Warehouse warehouse;
    private List<Warehouse> warehouseList;
    private List<Warehouse> filteredWarehouse;
    private Warehouse selectedWarehouse;

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public List<Warehouse> getWarehouseList() {
        if (warehouseList == null) {
            this.warehouseList = getWarehouseListFromDB();
        }
        return warehouseList;
    }

    public void setWarehouseList(List<Warehouse> warehouseList) {
        this.warehouseList = warehouseList;
    }

    public List<Warehouse> getFilteredWarehouse() {
        return filteredWarehouse;
    }

    public void setFilteredWarehouse(List<Warehouse> filteredWarehouse) {
        this.filteredWarehouse = filteredWarehouse;
    }

    public Warehouse getSelectedWarehouse() {
        return selectedWarehouse;
    }

    public void setSelectedWarehouse(Warehouse selectedWarehouse) {
        this.selectedWarehouse = selectedWarehouse;
    }

    public String addWarehouse() {
        try {
            warehouse.setEntityDataForAdd();
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            em.persist(warehouse);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nazwa nie jest unikalna", null));
            return "AddWarehouse";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie dodano magazyn"));
        warehouseList = getWarehouseListFromDB();
        return "WarehouseList";
    }

    public String editWarehouse() {
        try {
            selectedWarehouse.setEntityDataForEdit();
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            em.merge(selectedWarehouse);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nazwa nie jest unikalna", null));
            return "EditWarehouse";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie edytowano magazyn"));
        warehouseList = getWarehouseListFromDB();
        return "WarehouseList";
    }

    public String deleteWarehouse() {
        try {
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            Warehouse warehouseToDelete = em.find(Warehouse.class, selectedWarehouse.getId());
            selectedWarehouse = null;
            em.remove(warehouseToDelete);
            em.getTransaction().commit();
            em.close();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Błąd", "Nie można usunąć magazynu"));
            return "WarehouseList";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie usunięto magazyn"));
        warehouseList = getWarehouseListFromDB();
        return "WarehouseList";
    }

    public List<Warehouse> getWarehouseListFromDB() {
        EntityManager em = DBManager.getManager().createEntityManager();
        List list = em.createNamedQuery("Warehouse.findAll").getResultList();
        em.close();
        return list;
    }

    public String editWarehouseNavigation() {
        if (selectedWarehouse != null) {
            return "EditWarehouse";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Błąd", "Nie można edytować magazynu"));
            return "WarehouseList";
        }
    }

    public String addWarehouseNavigation() {
        warehouse = new Warehouse();
        return "AddWarehouse";
    }

    public String warehouseListNavigation() {
        warehouseList = getWarehouseListFromDB();
        return "WarehouseListRedirect";
    }
}
