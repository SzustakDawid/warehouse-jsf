package Warehouse.Controller;

import Warehouse.Config.DBManager;
import Warehouse.Exception.NoDocumentPositionException;
import Warehouse.Exception.SameProductException;
import Warehouse.Exception.SameWarehouseException;
import Warehouse.Exception.TooMuchCountException;
import Warehouse.Model.DocumentPosition;
import Warehouse.Model.MMDocument;
import Warehouse.Model.Product;
import Warehouse.Model.Stock;
import Warehouse.Model.Warehouse;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

public class MMDocumentBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private MMDocument mmDocument = new MMDocument();
    private List<MMDocument> mmDocumentList;
    private List<MMDocument> filteredMMDocument;
    private MMDocument selectedMMDocument;
    private Stock selectedStock;
    private String TxtWarehouseId;
    private List<Stock> filteredStock;
    private List<Stock> stockList;
    private Product productFromSelectedStock;

    public MMDocument getMmDocument() {
        return mmDocument;
    }

    public void setMmDocument(MMDocument mmDocument) {
        this.mmDocument = mmDocument;
    }

    public List<MMDocument> getMmDocumentList() {
        return mmDocumentList;
    }

    public void setMmDocumentList(List<MMDocument> mmDocumentList) {
        this.mmDocumentList = mmDocumentList;
    }

    public List<MMDocument> getFilteredMMDocument() {
        return filteredMMDocument;
    }

    public void setFilteredMMDocument(List<MMDocument> filteredMMDocument) {
        this.filteredMMDocument = filteredMMDocument;
    }

    public MMDocument getSelectedMMDocument() {
        return selectedMMDocument;
    }

    public void setSelectedMMDocument(MMDocument selectedMMDocument) {
        this.selectedMMDocument = selectedMMDocument;
    }

    public Stock getSelectedStock() {
        return selectedStock;
    }

    public void setSelectedStock(Stock selectedStock) {
        this.selectedStock = selectedStock;
        this.productFromSelectedStock = selectedStock.getProduct();
    }

    public String getTxtWarehouseId() {
        return TxtWarehouseId;
    }

    public void setTxtWarehouseId(String TxtWarehouseId) {
        this.TxtWarehouseId = TxtWarehouseId;
    }

    public List<Stock> getFilteredStock() {
        return filteredStock;
    }

    public void setFilteredStock(List<Stock> filteredStock) {
        this.filteredStock = filteredStock;
    }

    public List<Stock> getStockList() {
        return stockList;
    }

    public void setStockList(List<Stock> stockList) {
        this.stockList = stockList;
    }

    public Product getProductFromSelectedStock() {
        return productFromSelectedStock;
    }

    public void setProductFromSelectedStock(Product productFromSelectedStock) {
        this.productFromSelectedStock = productFromSelectedStock;
    }

    public String addMMDocumentNavigation() {
        mmDocument = new MMDocument();
        EntityManager em = DBManager.getManager().createEntityManager();
        List<Warehouse> list = em.createNamedQuery("Warehouse.findAll").setMaxResults(1).getResultList();
        if (!list.isEmpty()) {
            TxtWarehouseId = list.get(0).getId().toString();
        }
        getStockForWarehouseFromDB();
        return "AddMMDocument";
    }

    public String mmDocumentListNavigation() {
        mmDocumentList = getMMDocumentListFromDB();
        return "MMDocumentListRedirect";
    }

    public String viewMMDocumentNavigation() {
        if (selectedMMDocument != null) {
            return "ViewMMDocument";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Błąd", "Nie można pzejrzeć dokumentu"));
            return "MMDocumentList";
        }
    }

    public void removeSelectedStock() {
        selectedStock = null;
        productFromSelectedStock = null;
    }

    public final String getStockForWarehouseFromDB() {
        try {
            int warId = Integer.parseInt(TxtWarehouseId);
            EntityManager em = DBManager.getManager().createEntityManager();
            List list = em.createNamedQuery("Stock.findForWarehouse").setParameter("WarehouseId", warId).getResultList();
            em.close();
            stockList = list;
        } catch (Exception ex) {
            stockList = new ArrayList<Stock>();
        }
        return null;
    }

    public List<MMDocument> getMMDocumentListFromDB() {
        EntityManager em = DBManager.getManager().createEntityManager();
        List list = em.createNamedQuery("MMDocument.findAll").getResultList();
        em.close();
        return list;
    }

    public String AddMMDocument() {
        try {
            validateMMDocument();
            mmDocument.setEntityDataForAdd();
            mmDocument.setUser();
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            mmDocument.setDocumentNumber(CounterBean.CounterForMMDocument(em));
            em.persist(mmDocument);
            updateStocks(em);
            em.getTransaction().commit();
            em.close();
        } catch (SameProductException e) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Towary na liście muszą być unikalne", null));
            return "AddMMDocument";
        } catch (SameWarehouseException e) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Magazyn i magazyn docelowy, muszą być różne", null));
            return "AddMMDocument";
        } catch (TooMuchCountException e) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nie można wydać więcej towaru niż, jest w magazynie", null));
            return "AddMMDocument";
        } catch (NoDocumentPositionException e) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Dokument musi posiadać conajmniej jedną pozycję", null));
            return "AddMMDocument";
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nie udało się dodać dokumentu", null));
            return "AddMMDocument";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "OK", "Pomyślnie dodano dokument"));
        mmDocumentList = getMMDocumentListFromDB();
        return "MMDocumentList";
    }

    private void validateMMDocument() throws NoDocumentPositionException, TooMuchCountException, SameWarehouseException, SameProductException {
        List productList = new ArrayList<Product>();
        if (mmDocument.getDocumentPositions().isEmpty()) {
            throw new NoDocumentPositionException();
        } else if (mmDocument.getWarehouse().equals(mmDocument.getToWarehouse())) {
            throw new SameWarehouseException();
        } else {
            EntityManager em = DBManager.getManager().createEntityManager();
            for (DocumentPosition dp : mmDocument.getDocumentPositions()) {
                int productId = dp.getProduct().getId();
                int warehouseId = mmDocument.getWarehouse().getId();
                List<Stock> list = em.createNamedQuery("Stock.findByWarehouseAndProduct").setParameter("WarehouseId", warehouseId).setParameter("ProductId", productId).getResultList();
                if (list.get(0).getQuantity() < dp.getCount()) {
                    throw new TooMuchCountException();
                }
                if (!productList.contains(dp.getProduct())) {
                    productList.add(dp.getProduct());
                } else {
                    throw new SameProductException();
                }
            }
        }
    }

    public void updateStocks(EntityManager em) {
        Warehouse warehouse = mmDocument.getWarehouse();
        Warehouse toWarehouse = mmDocument.getToWarehouse();
        for (DocumentPosition documentPosition : mmDocument.getDocumentPositions()) {
            List<Stock> stockList = em.createNamedQuery("Stock.findByWarehouseAndProduct").setParameter("WarehouseId", warehouse.getId()).setParameter("ProductId", documentPosition.getProduct().getId()).getResultList();
            if (stockList.size() > 0) {
                Stock stock = stockList.get(0);
                int actual = stock.getQuantity();
                actual -= documentPosition.getCount();
                stock.setQuantity(actual);
                em.merge(stock);
                List<Stock> stockTo = em.createNamedQuery("Stock.findByWarehouseAndProduct").setParameter("WarehouseId", toWarehouse.getId()).setParameter("ProductId", documentPosition.getProduct().getId()).getResultList();
                if (stockTo.size() > 0) {
                    Stock stockToAdd = stockTo.get(0);
                    int actualCount = stockToAdd.getQuantity();
                    actualCount += documentPosition.getCount();
                    stockToAdd.setQuantity(actualCount);
                    em.merge(stockToAdd);
                } else {
                    Stock stockToAdd = new Stock(toWarehouse, documentPosition.getProduct(), documentPosition.getCount());
                    em.persist(stockToAdd);
                }
            }
        }
        StockBean.deleteNoQuantity(em);
    }
}
