package Warehouse.Controller;

import Warehouse.Config.DBManager;
import Warehouse.Model.Stock;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

public class StockBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private int WarehouseId;
    private List<Stock> stockList;
    private List<Stock> filteredStock;

    public int getWarehouseId() {
        return WarehouseId;
    }

    public void setWarehouseId(int WarehouseId) {
        this.WarehouseId = WarehouseId;
    }

    public List<Stock> getStockList() {
        if (stockList == null) {
            stockList = getStockListFromDB(WarehouseId);
        }
        return stockList;
    }

    public void setStockList(List<Stock> stockList) {
        this.stockList = stockList;
    }

    public List<Stock> getFilteredStock() {
        return filteredStock;
    }

    public void setFilteredStock(List<Stock> filteredStock) {
        this.filteredStock = filteredStock;
    }

    public List<Stock> getStockListFromDB(int WarehouseId) {
        EntityManager em = DBManager.getManager().createEntityManager();
        List list = em.createNamedQuery("Stock.findForWarehouse").setParameter("WarehouseId", WarehouseId).getResultList();
        em.close();
        return list;
    }

    public String viewStock() {
        if (WarehouseId != 0) {
            stockList = getStockListFromDB(WarehouseId);
            return "StockList?faces-redirect=true";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Błąd", "Nie można przejrzeć magazynu"));
            return "WarehouseList";
        }
    }

    public static void deleteNoQuantity(EntityManager em) {
        List<Stock> list = em.createNamedQuery("Stock.findNoQuantity").getResultList();

        for (Stock stock : list) {
            deleteStock(stock, em);
        }
    }

    private static void deleteStock(Stock stock, EntityManager em) {
        try {
            Stock stockToDelete = em.find(Stock.class, stock.getId());
            em.remove(stockToDelete);
        } catch (Exception ex) {
        }
    }
}
