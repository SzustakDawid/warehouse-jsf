package Warehouse.Controller;

import Warehouse.Model.Counter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.EntityManager;

public class CounterBean implements Serializable {

    private static final long serialVersionUID = 1L;

    public static String CounterForPZDocument(EntityManager em) {
        String documentNumber;
        Calendar cal = new GregorianCalendar();
        List<Counter> list = em.createNamedQuery("Counter.findByName").setParameter("DocumentName", "PZDocument").getResultList();
        if (!list.isEmpty()) {
            Counter counter = list.get(0);
            int actualValue = counter.getActualValue();
            actualValue++;
            counter.setActualValue(actualValue);
            counter.setDocumentName("PZDocument");
            documentNumber = "PZ\\" + actualValue + "\\" + (cal.get(Calendar.MONTH) + 1) + "\\" + (cal.get(Calendar.YEAR));
            em.merge(counter);
        } else {
            Counter counter = new Counter();
            counter.setActualValue(1);
            counter.setDocumentName("PZDocument");
            documentNumber = "PZ\\" + 1 + "\\" + (cal.get(Calendar.MONTH) + 1) + "\\" + (cal.get(Calendar.YEAR));
            em.persist(counter);
        }
        return documentNumber;
    }

    public static String CounterForWZDocument(EntityManager em) {
        String documentNumber;
        Calendar cal = new GregorianCalendar();
        List<Counter> list = em.createNamedQuery("Counter.findByName").setParameter("DocumentName", "WZDocument").getResultList();
        if (!list.isEmpty()) {
            Counter counter = list.get(0);
            int actualValue = counter.getActualValue();
            actualValue++;
            counter.setActualValue(actualValue);
            counter.setDocumentName("WZDocument");
            documentNumber = "WZ\\" + actualValue + "\\" + (cal.get(Calendar.MONTH) + 1) + "\\" + (cal.get(Calendar.YEAR));
            em.merge(counter);
        } else {
            Counter counter = new Counter();
            counter.setActualValue(1);
            counter.setDocumentName("WZDocument");
            documentNumber = "WZ\\" + 1 + "\\" + (cal.get(Calendar.MONTH) + 1) + "\\" + (cal.get(Calendar.YEAR));
            em.persist(counter);
        }
        return documentNumber;
    }

    public static String CounterForMMDocument(EntityManager em) {
        String documentNumber;
        Calendar cal = new GregorianCalendar();
        List<Counter> list = em.createNamedQuery("Counter.findByName").setParameter("DocumentName", "MMDocument").getResultList();
        if (!list.isEmpty()) {
            Counter counter = list.get(0);
            int actualValue = counter.getActualValue();
            actualValue++;
            counter.setActualValue(actualValue);
            counter.setDocumentName("MMDocument");
            documentNumber = "MM\\" + actualValue + "\\" + (cal.get(Calendar.MONTH) + 1) + "\\" + (cal.get(Calendar.YEAR));
            em.merge(counter);
        } else {
            Counter counter = new Counter();
            counter.setActualValue(1);
            counter.setDocumentName("MMDocument");
            documentNumber = "MM\\" + 1 + "\\" + (cal.get(Calendar.MONTH) + 1) + "\\" + (cal.get(Calendar.YEAR));
            em.persist(counter);
        }
        return documentNumber;
    }
}
