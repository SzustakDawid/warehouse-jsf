package Warehouse.Config;

import java.io.Serializable;

public class RoleNames implements Serializable {

    private final String DodajPodatek = "Dodaj Podatek";
    private final String UsunPodatek = "Usun Podatek";
    private final String DodajJednostke = "Dodaj Jenostke";
    private final String UsunJednostke = "Usun Jednostke";
    private final String DodajUzytkownika = "Dodaj Uzytkownika";
    private final String EdytujUzytkownika = "Edytuj Uzytkownika";
    private final String UsunUzytkownika = "Usun Uzytkownika";
    private final String DodajKontrahenta = "Dodaj Kontrahenta";
    private final String EdytujKontrahenta = "Edytuj Kontrahenta";
    private final String UsunKontrahenta = "Usun Kontrahenta";
    private final String DodajTowar = "Dodaj Towar";
    private final String EdytujTowar = "Edytuj Towar";
    private final String UsunTowar = "Usun Towar";
    private final String DodajMagazyn = "Dodaj Magazyn";
    private final String EdytujMagazyn = "Edytuj Magazyn";
    private final String UsunMagazyn = "Usun Magazyn";
    private final String Uprawnienia = "Uprawnienia";
    private final String DodajDokumentPZ = "Dodaj Dokument PZ";
    private final String PrzegladajDokumentPZ = "Przegladaj Dokument PZ";
    private final String DodajDokumentWZ = "Dodaj Dokument WZ";
    private final String PrzegladajDokumentWZ = "Przegladaj Dokument WZ";
    private final String DodajDokumentMM = "Dodaj Dokument MM";
    private final String PrzegladajDokumentMM = "Przegladaj Dokument MM";

    public String getDodajPodatek() {
        return DodajPodatek;
    }

    public String getUsunPodatek() {
        return UsunPodatek;
    }

    public String getDodajJednostke() {
        return DodajJednostke;
    }

    public String getUsunJednostke() {
        return UsunJednostke;
    }

    public String getDodajUzytkownika() {
        return DodajUzytkownika;
    }

    public String getEdytujUzytkownika() {
        return EdytujUzytkownika;
    }

    public String getUsunUzytkownika() {
        return UsunUzytkownika;
    }

    public String getDodajKontrahenta() {
        return DodajKontrahenta;
    }

    public String getEdytujKontrahenta() {
        return EdytujKontrahenta;
    }

    public String getUsunKontrahenta() {
        return UsunKontrahenta;
    }

    public String getDodajTowar() {
        return DodajTowar;
    }

    public String getEdytujTowar() {
        return EdytujTowar;
    }

    public String getUsunTowar() {
        return UsunTowar;
    }

    public String getDodajMagazyn() {
        return DodajMagazyn;
    }

    public String getEdytujMagazyn() {
        return EdytujMagazyn;
    }

    public String getUsunMagazyn() {
        return UsunMagazyn;
    }

    public String getUprawnienia() {
        return Uprawnienia;
    }

    public String getDodajDokumentPZ() {
        return DodajDokumentPZ;
    }

    public String getPrzegladajDokumentPZ() {
        return PrzegladajDokumentPZ;
    }

    public String getDodajDokumentWZ() {
        return DodajDokumentWZ;
    }

    public String getPrzegladajDokumentWZ() {
        return PrzegladajDokumentWZ;
    }

    public String getDodajDokumentMM() {
        return DodajDokumentMM;
    }

    public String getPrzegladajDokumentMM() {
        return PrzegladajDokumentMM;
    }
}
