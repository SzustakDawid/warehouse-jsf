package Warehouse.Validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;


public class NipValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        FacesMessage msg = new FacesMessage("Niepoprawny numer NIP");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);

        String nip = value.toString();
        nip = nip.replaceAll("-", "");

        int[] weights = {6, 5, 7, 2, 3, 4, 5, 6, 7};
        String[] aNip = nip.split("");

        try {
            int sum = 0;
            for (int i = 0; i < weights.length; i++) {

                sum += Integer.parseInt(aNip[i + 1]) * weights[i];
            }
            if ((sum % 11) == Integer.parseInt(aNip[10]) == false) {
                throw new ValidatorException(msg);
            }
        } catch (NumberFormatException e) {
            throw new ValidatorException(msg);
        }
    }
}
