package Warehouse.Validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class PriceValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {

        FacesMessage msg = new FacesMessage("Cena nie może być ujemna");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);

        String v = String.valueOf(o);
        Float value = Float.parseFloat(v);
        if (value < 0) {
            throw new ValidatorException(msg);
        }
    }
}
