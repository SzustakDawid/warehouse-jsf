package Warehouse.Validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class TaxValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {

        FacesMessage msg = new FacesMessage("Podatek nie może być ujemny");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);

        String v = String.valueOf(o);
        int value = Integer.parseInt(v);
        if (value < 0) {
            throw new ValidatorException(msg);
        }
    }
}
