package Warehouse.Validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class PasswordValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        String passwordId = (String) component.getAttributes().get("passwordId");
        UIInput passwordInput = (UIInput) context.getViewRoot().findComponent(passwordId);
        String password = (String) passwordInput.getValue();
        String confirm = (String) value;

        if (!password.equals(confirm) && password.length() > 4) {
            ((UIInput) component).setValid(false);
            ((UIInput) passwordInput).setValue("");
            FacesContext.getCurrentInstance().addMessage("msgs", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Hasła nie są identyczne", null));
        } else if (confirm.length() < 5) {
            ((UIInput) component).setValid(false);
        }
    }
}
