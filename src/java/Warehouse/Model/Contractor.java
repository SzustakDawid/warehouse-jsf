package Warehouse.Model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "Contractors")
@NamedQueries({
    @NamedQuery(name = "Contractor.findById", query = "SELECT c FROM Contractor c WHERE c.Id = :Id"),
    @NamedQuery(name = "Contractor.findAll", query = "SELECT c FROM Contractor c"),
    @NamedQuery(name = "Contractor.findSeller", query = "SELECT c FROM Contractor c WHERE c.Seller = TRUE"),
    @NamedQuery(name = "Contractor.findBuyer", query = "SELECT c FROM Contractor c WHERE c.Buyer = TRUE")})
public class Contractor extends EntityBase implements Serializable {

    private static final long serialVersionUID = 1L;

    public Contractor() {
        this.Country = "Polska";
    }
    @Column(name = "Name", unique = true)
    private String Name;
    @Column(name = "FullName")
    private String FullName;
    @Column(name = "PhoneNumber")
    private String PhoneNumber;
    @Column(name = "Fax")
    private String Fax;
    @Column(name = "MobilePhone")
    private String MobilePhone;
    @Column(name = "Mail")
    private String Mail;
    @Column(name = "Street")
    private String Street;
    @Column(name = "City")
    private String City;
    @Column(name = "Country")
    private String Country;
    @Column(name = "Province")
    private String Province;
    @Column(name = "PostCode")
    private String PostCode;
    @Column(name = "Nip")
    private String Nip;
    @Column(name = "Regon")
    private String Regon;
    @Lob
    @Column(name = "AdditionalInformation")
    private String AdditionalInformation;
    @Column(name = "Seller")
    private boolean Seller;
    @Column(name = "Buyer")
    private boolean Buyer;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String FullName) {
        this.FullName = FullName;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String PhoneNumber) {
        this.PhoneNumber = PhoneNumber;
    }

    public String getMobilePhone() {
        return MobilePhone;
    }

    public void setMobilePhone(String MobilePhone) {
        this.MobilePhone = MobilePhone;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String Mail) {
        this.Mail = Mail;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String Street) {
        this.Street = Street;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String Province) {
        this.Province = Province;
    }

    public String getNip() {
        return Nip;
    }

    public void setNip(String Nip) {
        this.Nip = Nip;
    }

    public String getRegon() {
        return Regon;
    }

    public void setRegon(String Regon) {
        this.Regon = Regon;
    }

    public boolean isSeller() {
        return Seller;
    }

    public void setSeller(boolean Seller) {
        this.Seller = Seller;
    }

    public boolean isBuyer() {
        return Buyer;
    }

    public void setBuyer(boolean Buyer) {
        this.Buyer = Buyer;
    }

    public String getAdditionalInformation() {
        return AdditionalInformation;
    }

    public void setAdditionalInformation(String AdditionalInformation) {
        this.AdditionalInformation = AdditionalInformation;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String Fax) {
        this.Fax = Fax;
    }

    public String getPostCode() {
        return PostCode;
    }

    public void setPostCode(String PostCode) {
        this.PostCode = PostCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Contractor)) {
            return false;
        }
        Contractor other = (Contractor) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }
}
