package Warehouse.Model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Entity
@Table(name = "WZDocuments")
@NamedQueries({
    @NamedQuery(name = "WZDocument.findAll", query = "SELECT w FROM WZDocument w")})
public class WZDocument extends Document implements Serializable {

    private static final long serialVersionUID = 1L;

    public WZDocument() {
        Date date = new Date();
        DateOfWarehouseRelease = date;
    }
    @Temporal(TemporalType.DATE)
    @Column(name = "DateOfWarehouseRelease")
    private Date DateOfWarehouseRelease;

    public Date getDateOfWarehouseRelease() {
       return new Date(DateOfWarehouseRelease.getTime() + TimeUnit.HOURS.toMillis(6));
    }

    public void setDateOfWarehouseRelease(Date DateOfWarehouseRelease) {
        this.DateOfWarehouseRelease = DateOfWarehouseRelease;
    }
}