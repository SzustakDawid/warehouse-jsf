package Warehouse.Model;

import Warehouse.Controller.LoginBean;
import java.io.Serializable;
import java.util.Date;
import javax.faces.context.FacesContext;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.servlet.http.HttpServletRequest;

@Embeddable
@MappedSuperclass
public class EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    protected Integer Id;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedAt")
    protected Date CreatedAt;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UpdatedAt")
    protected Date UpdatedAt;
    @OneToOne
    @JoinColumn(name = "CreatedBy_Id", nullable = true)
    protected User CreatedBy;
    @OneToOne
    @JoinColumn(name = "UpdatedBy_Id", nullable = true)
    protected User UpdatedBy;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Date getCreatedAt() {
        return CreatedAt;
    }

    public Date getUpdatedAt() {
        return UpdatedAt;
    }

    public User getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(User CreatedBy) {
        this.CreatedBy = CreatedBy;
    }

    public User getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(User UpdatedBy) {
        this.UpdatedBy = UpdatedBy;
    }

    public void setCreatedAt(Date CreatedAt) {
        this.CreatedAt = CreatedAt;
    }

    public void setUpdatedAt(Date UpdatedAt) {
        this.UpdatedAt = UpdatedAt;
    }

    public void setEntityDataForAdd() {
        Date date = new Date();
        this.CreatedAt = date;
        this.UpdatedAt = date;
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        LoginBean loginBean = (LoginBean) req.getSession().getAttribute("LoginBean");
        User user = loginBean.getCurrentUser();
        this.CreatedBy = user;
        this.UpdatedBy = user;
    }

    public void setEntityDataForEdit() {
        Date date = new Date();
        this.UpdatedAt = date;
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        LoginBean loginBean = (LoginBean) req.getSession().getAttribute("LoginBean");
        User user = loginBean.getCurrentUser();
        this.UpdatedBy = user;
    }
}
