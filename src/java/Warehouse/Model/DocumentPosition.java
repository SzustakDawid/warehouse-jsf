package Warehouse.Model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "DocumentPositions")
public class DocumentPosition implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    protected Integer Id;
    @OneToOne
    @JoinColumn(name = "Product_Id", nullable = false)
    private Product Product;
    @Column(name = "Price")
    private float Price;
    @Column(name = "Count")
    private int Count;
    @Column(name = "Tax")
    private int Tax;
    
    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Product getProduct() {
        return Product;
    }

    public void setProduct(Product Product) {
        this.Product = Product;
    }

    public float getPrice() {
        if (Price <= 0 && getProduct() != null) {
            this.setPrice(getProduct().getPrice());
        }
        return Price;
    }

    public void setPrice(float Price) {
        this.Price = Price;
    }

    public int getCount() {
        if (Count <= 0) {
            this.setCount(1);
        }
        return Count;
    }

    public void setCount(int Count) {
        this.Count = Count;
    }

    public int getTax() {
        return Tax;
    }

    public void setTax(int Tax) {
        this.Tax = Tax;
    } 
}
