package Warehouse.Model;

import Warehouse.Controller.LoginBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "Documents")
@Inheritance(strategy = InheritanceType.JOINED)
public class Document extends EntityBase implements Serializable {

    private static final long serialVersionUID = 1L;

    public Document() {
        Date date = new Date();
        DateOfDocument = date;
    }
    @OneToOne
    @JoinColumn(name = "Contractor_Id", nullable = true)
    private Contractor Contractor;
    @OneToOne
    @JoinColumn(name = "User_Id", nullable = false)
    private User User;
    @OneToOne
    @JoinColumn(name = "Warehouse_Id", nullable = false)
    private Warehouse Warehouse;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DateOfDocument")
    private Date DateOfDocument;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "Document_Id")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<DocumentPosition> DocumentPositions = new ArrayList<DocumentPosition>();
    @Column(name = "DocumentNumber")
    private String DocumentNumber;

    public List<DocumentPosition> getDocumentPositions() {
        return DocumentPositions;
    }

    public void setDocumentPositions(List<DocumentPosition> DocumentPositions) {
        this.DocumentPositions = DocumentPositions;
    }

    public Contractor getContractor() {
        return Contractor;
    }

    public void setContractor(Contractor Contractor) {
        this.Contractor = Contractor;
    }

    public User getUser() {
        return User;
    }

    public void setUser() {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        LoginBean loginBean = (LoginBean) req.getSession().getAttribute("LoginBean");
        User user = loginBean.getCurrentUser();
        this.User = user;
    }

    public Warehouse getWarehouse() {
        return Warehouse;
    }

    public void setWarehouse(Warehouse Warehouse) {
        this.Warehouse = Warehouse;
    }

    public Date getDateOfDocument() {
        return DateOfDocument;
    }

    public void setDateOfDocument(Date DateOfDocument) {
        this.DateOfDocument = DateOfDocument;
    }

    public String getDocumentNumber() {
        return DocumentNumber;
    }

    public void setDocumentNumber(String DocumentNumber) {
        this.DocumentNumber = DocumentNumber;
    }
}
