package Warehouse.Model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "Counters")
@NamedQueries({
    @NamedQuery(name = "Counter.findAll", query = "SELECT c FROM Counter c"),
    @NamedQuery(name = "Counter.findByName", query = "SELECT c FROM Counter c WHERE c.DocumentName = :DocumentName")
})
public class Counter implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer Id;
    @Column(name = "DocumentName")
    private String DocumentName;
    @Column(name = "ActualValue")
    private int ActualValue;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }
    
    public String getDocumentName() {
        return DocumentName;
    }

    public void setDocumentName(String DocumentName) {
        this.DocumentName = DocumentName;
    }

    public int getActualValue() {
        return ActualValue;
    }

    public void setActualValue(int ActualValue) {
        this.ActualValue = ActualValue;
    }
}
