package Warehouse.Model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Entity
@Table(name = "PZDocuments")
@NamedQueries({
    @NamedQuery(name = "PZDocument.findAll", query = "SELECT p FROM PZDocument p")})
public class PZDocument extends Document implements Serializable {

    private static final long serialVersionUID = 1L;

    public PZDocument() {
        Date date = new Date();
        BuyDate = date;
    }
    @Temporal(TemporalType.DATE)
    @Column(name = "BuyDate")
    private Date BuyDate;

    public Date getBuyDate() {
        return new Date(BuyDate.getTime() + TimeUnit.HOURS.toMillis(6));
    }

    public void setBuyDate(Date BuyDate) {
        this.BuyDate = BuyDate;
    }
}
