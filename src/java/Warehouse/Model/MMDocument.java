package Warehouse.Model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MMDocuments")
@NamedQueries({
    @NamedQuery(name = "MMDocument.findAll", query = "SELECT m FROM MMDocument m")})
public class MMDocument extends Document implements Serializable {

    private static final long serialVersionUID = 1L;
    @OneToOne
    @JoinColumn(name = "ToWarehouse_Id", nullable = false)
    private Warehouse ToWarehouse;

    public Warehouse getToWarehouse() {
        return ToWarehouse;
    }

    public void setToWarehouse(Warehouse ToWarehouse) {
        this.ToWarehouse = ToWarehouse;
    }
}