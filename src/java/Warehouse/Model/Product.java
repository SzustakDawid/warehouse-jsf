package Warehouse.Model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "products")
@NamedQueries({
    @NamedQuery(name = "Products.findAll", query = "SELECT p FROM Product p")})
public class Product extends EntityBase implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "Symbol", unique = true)
    private String Symbol;
    @Column(name = "Name")
    private String Name;
    @Lob
    @Column(name = "Description")
    private String Description;
    @Column(name = "Price")
    private float Price;
    @OneToOne
    @JoinColumn(name = "Tax_Id", nullable = false)
    private Tax Tax;
    @OneToOne
    @JoinColumn(name = "Unit_Id", nullable = false)
    private Unit Unit;

    public String getSymbol() {
        return Symbol;
    }

    public void setSymbol(String Symbol) {
        this.Symbol = Symbol;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public float getPrice() {
        return Price;
    }

    public void setPrice(float Price) {
        this.Price = Price;
    }

    public Tax getTax() {
        return Tax;
    }

    public void setTax(Tax Tax) {
        this.Tax = Tax;
    }

    public Unit getUnit() {
        return Unit;
    }

    public void setUnit(Unit Unit) {
        this.Unit = Unit;
    }
}
