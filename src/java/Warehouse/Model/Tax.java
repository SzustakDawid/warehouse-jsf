package Warehouse.Model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "Tax")
@NamedQueries({
    @NamedQuery(name = "Tax.findById", query = "SELECT t FROM Tax t WHERE t.Id  = :Id"),
    @NamedQuery(name = "Tax.findAll", query = "SELECT t FROM Tax t")})
public class Tax extends EntityBase implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "Tax", unique = true)
    private int Tax;

    public int getTax() {
        return Tax;
    }

    public void setTax(int Tax) {
        this.Tax = Tax;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Tax)) {
            return false;
        }
        Tax other = (Tax) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }
}
