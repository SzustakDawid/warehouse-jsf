package Warehouse.Model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Stocks")
@NamedQueries({
    @NamedQuery(name = "Stock.findAll", query = "SELECT s FROM Stock s"),
    @NamedQuery(name = "Stock.findNoQuantity", query = "SELECT s FROM Stock s WHERE s.Quantity = 0"),
    @NamedQuery(name = "Stock.findForWarehouse", query = "SELECT s FROM Stock s WHERE s.Warehouse.Id = :WarehouseId"),
    @NamedQuery(name = "Stock.findByWarehouseAndProduct", query = "SELECT s FROM Stock s WHERE s.Warehouse.Id = :WarehouseId AND s.Product.Id = :ProductId")
})
public class Stock implements Serializable {

    private static final long serialVersionUID = 1L;

    public Stock() {
    }

    public Stock(Warehouse warehouse, Product product, int count) {
        this.Warehouse = warehouse;
        this.Product = product;
        this.Quantity = count;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Integer Id;
    @OneToOne
    @JoinColumn(name = "Warehouse_Id", nullable = false)
    private Warehouse Warehouse;
    @OneToOne
    @JoinColumn(name = "Product_Id", nullable = false)
    private Product Product;
    @Column(name = "Quantity")
    private int Quantity;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }
    
    public Warehouse getWarehouse() {
        return Warehouse;
    }

    public void setWarehouse(Warehouse Warehouse) {
        this.Warehouse = Warehouse;
    }

    public Product getProduct() {
        return Product;
    }

    public void setProduct(Product Product) {
        this.Product = Product;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int Quantity) {
        this.Quantity = Quantity;
    }
}
