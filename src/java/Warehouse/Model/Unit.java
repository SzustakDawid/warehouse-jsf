package Warehouse.Model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "Units")
@NamedQueries({
    @NamedQuery(name = "Unit.findById", query = "Select u FROM Unit u WHERE u.Id = :Id"),
    @NamedQuery(name = "Unit.findAll", query = "SELECT u FROM Unit u")})
public class Unit extends EntityBase implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "Name", unique = true)
    private String Name;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (!(object instanceof Unit)) {
            return false;
        }
        Unit other = (Unit) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }
}
