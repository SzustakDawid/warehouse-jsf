package Warehouse.Converter;

import Warehouse.Config.DBManager;
import Warehouse.Model.Tax;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.persistence.EntityManager;

public class TaxConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        Tax tax = null;
        try {
            int objectId = Integer.parseInt(string);
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            tax = (Tax) em.createNamedQuery("Tax.findById").setParameter("Id", objectId).getSingleResult();
            em.getTransaction().commit();
            em.close();
        } catch (Throwable ex) {
            throw new ConverterException("Wystąpił błąd podczas konwersji.");
        }
        return tax;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        String val = null;
        try {
            Tax tax = (Tax) o;
            val = Integer.toString(tax.getId());
        } catch (Throwable ex) {
            throw new ConverterException("Wystąpił błąd podczas konwersji.");
        }
        return val;
    }
}
