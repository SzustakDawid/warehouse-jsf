package Warehouse.Converter;

import Warehouse.Config.DBManager;
import Warehouse.Model.Unit;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.persistence.EntityManager;

public class UnitConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        Unit unit = null;
        try {
            int objectId = Integer.parseInt(string);
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            unit = (Unit) em.createNamedQuery("Unit.findById").setParameter("Id", objectId).getSingleResult();
            em.getTransaction().commit();
            em.close();
        } catch (Throwable ex) {
            throw new ConverterException("Wystąpił błąd podczas konwersji.");
        }
        return unit;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        String val = null;
        try {
            Unit unit = (Unit) o;
            val = Integer.toString(unit.getId());
        } catch (Throwable ex) {
            throw new ConverterException("Wystąpił błąd podczas konwersji.");
        }
        return val;
    }
}
