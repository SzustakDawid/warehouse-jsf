package Warehouse.Converter;

import Warehouse.Config.DBManager;
import Warehouse.Model.Contractor;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.persistence.EntityManager;

public class ContractorConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        Contractor contractor = null;
        try {
            int objectId = Integer.parseInt(string);
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            contractor = (Contractor) em.createNamedQuery("Contractor.findById").setParameter("Id", objectId).getSingleResult();
            em.getTransaction().commit();
            em.close();
        } catch (Throwable ex) {
            throw new ConverterException("Wystąpił błąd podczas konwersji.");
        }
        return contractor;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        String val = null;
        try {
            Contractor contractor = (Contractor) o;
            val = Integer.toString(contractor.getId());
        } catch (Throwable ex) {
            throw new ConverterException("Wystąpił błąd podczas konwersji.");
        }
        return val;
    }
}
