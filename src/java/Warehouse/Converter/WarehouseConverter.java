package Warehouse.Converter;

import Warehouse.Config.DBManager;
import Warehouse.Model.Warehouse;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.persistence.EntityManager;

public class WarehouseConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        Warehouse warehouse = null;
        try {
            int objectId = Integer.parseInt(string);
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            warehouse = (Warehouse) em.createNamedQuery("Warehouse.findById").setParameter("Id", objectId).getSingleResult();
            em.getTransaction().commit();
            em.close();
        } catch (Throwable ex) {
            throw new ConverterException("Wystąpił błąd podczas konwersji.");
        }
        return warehouse;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        String val = null;
        try {
            Warehouse warehouse = (Warehouse) o;
            val = Integer.toString(warehouse.getId());
        } catch (Throwable ex) {
            throw new ConverterException("Wystąpił błąd podczas konwersji.");
        }
        return val;
    }
}
