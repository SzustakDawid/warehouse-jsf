package Warehouse.Converter;

import Warehouse.Config.DBManager;
import Warehouse.Model.User;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.persistence.EntityManager;

public class UserConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        User user = null;
        try {
            int objectId = Integer.parseInt(string);
            EntityManager em = DBManager.getManager().createEntityManager();
            em.getTransaction().begin();
            user = (User) em.createNamedQuery("User.findById").setParameter("Id", objectId).getSingleResult();
            em.getTransaction().commit();
            em.close();
        } catch (Throwable ex) {
            throw new ConverterException("Wystąpił błąd podczas konwersji.");
        }
        return user;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        String val = null;
        try {
            User user = (User) o;
            val = Integer.toString(user.getId());
        } catch (Throwable ex) {
            throw new ConverterException("Wystąpił błąd podczas konwersji.");
        }
        return val;
    }
}
