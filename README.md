# **System Magazynowy** #

Aplikacja webowa napisana w języku Java w technologii **Java Server Faces** jako praca inżynierska.  

  
**Wymagania funkcjonalne:**  

- Logowanie do systemu  
- Dodawanie, edytowanie, usuwanie oraz przeglądanie kontrahentów  
- Dodawanie, edytowanie, usuwanie oraz przeglądania towarów  
- Dodawanie, edytowanie, usuwanie oraz przeglądanie magazynów  
- Przeglądanie aktualnych stanów magazynowych  
- Dodawanie, edytowanie, usuwanie oraz przeglądanie użytkowników systemu  
- Nadawanie uprawnień użytkownikom  
- Definiowanie wysokości podatków dla towarów   
- Definiowanie jednostek dla towarów  
- Dodawanie oraz przeglądanie przyjęć magazynowych  
- Dodawanie oraz przeglądanie wydań magazynowych  
- Dodawanie oraz przeglądanie przemieszczeń między-magazynowych  
- Zmianę hasła aktualnie zalogowanego użytkownika  
- Zmianę danych aktualnie zalogowanego użytkownika  
- Wylogowanie z systemu  
  
**Diagram Klas:**  
![Diagram klas.png](https://bitbucket.org/repo/9RAknb/images/2680862089-Diagram%20klas.png)

**Screeny aplikacji:**  
  
![1.png](https://bitbucket.org/repo/9RAknb/images/1560884352-1.png)  
![2.png](https://bitbucket.org/repo/9RAknb/images/384400424-2.png)  
![3.png](https://bitbucket.org/repo/9RAknb/images/2824136800-3.png)  
![4.png](https://bitbucket.org/repo/9RAknb/images/3316144472-4.png)